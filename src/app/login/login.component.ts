import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Socket } from 'ngx-socket-io';
import { AuthApiService } from '../services/auth-api.service';
import { MatSnackBar } from '@angular/material';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
    constructor(private router: Router, private socket: Socket, private api: AuthApiService, private snackBar: MatSnackBar) { }

    username: string;
    password: string;

    ngOnInit() {
        this.socket.on('new_order', (data: any) => {
            console.log(data);
        });
    }

    onLogin() {
        const body = {
            username: this.username,
            password: this.password,
        };
        this.api.login(body).subscribe(result => {
            if (result.success) {
                localStorage.setItem('token', result.result.jwt);
                this.router.navigate(['/dashboard']);
            } else {
                this.snackBar.open(result.message);
            }
        });
        return false;
    }
}
