import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Socket } from 'ngx-socket-io';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
    constructor(private translate: TranslateService, private socket: Socket) {
        translate.setDefaultLang('en');
    }

    ngOnInit() {
        // this.socket.on('connect', () => {
        //     this.socket.emit('authenticate', { token: localStorage.getItem('token') });
        // });
    }
}
