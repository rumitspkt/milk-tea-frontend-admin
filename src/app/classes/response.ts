export interface DataResponse {
    success: boolean;
    result: any;
    message: string;
}
