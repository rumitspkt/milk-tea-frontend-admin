export class Order {
    _id: string;
    user_id: string;
    shipper_id: string;
    status: string;
    address: string;
    lat_addr: number;
    long_addr: number;
    note: number;

}
