export class MainDrink {
    _id: string;
    name: string;
    price: number;
    image_url: string;
}
