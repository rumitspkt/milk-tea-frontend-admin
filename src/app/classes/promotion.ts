export class Promotion {
    _id: string;
    code: string;
    title: string;
    content: string;
    image_url: string;
    time_start: string;
    time_end: string;
    value: number;

}
