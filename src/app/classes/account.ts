export class Account {
    _id: string;
    username: string;
    role: string;
    phone: string;
    address: string;
    email: string;
    password: string;
}
