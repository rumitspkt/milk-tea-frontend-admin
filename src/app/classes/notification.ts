export class Notification {
    _id: string;
    title: string;
    content: string;
    image_url: string;
}
