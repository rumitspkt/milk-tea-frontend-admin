export class Category
{
    _id: string;
    name: string;
    code: string;
    image_url: string;
}