export class Topping 
{
    _id: string;
    code: string;
    name: string;
    price: number;
    is_active: boolean;
}