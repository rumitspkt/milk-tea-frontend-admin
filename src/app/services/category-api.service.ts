import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { BaseService } from '../shared/services';
import { DataResponse } from '../classes/response';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CategoryApiService extends BaseService {

  constructor(private httpClient: HttpClient) {
    super();
  }

  getCategory(page: number, pageSize: number): Observable<DataResponse> {
    const url = `${environment.baseUrl}/admin/category?page=${page}&pageSize=${pageSize}`;
    return this.httpClient.get<DataResponse>(url).pipe(catchError(this.handleError));
  }

  createCategory(data: any): Observable<DataResponse> {
    return this.httpClient.post<DataResponse>(`${environment.baseUrl}/admin/category`, data, {})
      .pipe(catchError(this.handleError));
  }

  editCategory(data: any): Observable<DataResponse> {
    return this.httpClient.put<DataResponse>(`${environment.baseUrl}/admin/category`, data, {})
      .pipe(catchError(this.handleError));
  }

  deleteCategory(data: any): Observable<DataResponse> {
    return this.httpClient.request<DataResponse>('delete', `${environment.baseUrl}/admin/category`, { body: data })
      .pipe(catchError(this.handleError));
  }
}
