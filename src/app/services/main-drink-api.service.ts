import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { BaseService } from '../shared/services';
import { DataResponse } from '../classes/response';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class MainDrinkApiService extends BaseService {

  constructor(private httpClient: HttpClient) {
    super();
  }

  getMainDrink(categoryId: string): Observable<DataResponse> {
    const url = `${environment.baseUrl}/admin/main_drink/${categoryId}`;
    return this.httpClient.get<DataResponse>(url).pipe(catchError(this.handleError));
  }

  createMainDrink(data: any): Observable<DataResponse> {
    return this.httpClient.post<DataResponse>(`${environment.baseUrl}/admin/main_drink`, data, {})
      .pipe(catchError(this.handleError));
  }

  editMainDrink(data: any): Observable<DataResponse> {
    return this.httpClient.put<DataResponse>(`${environment.baseUrl}/admin/main_drink`, data, {})
      .pipe(catchError(this.handleError));
  }

  deleteMainDrink(data: any): Observable<DataResponse> {
    return this.httpClient.request<DataResponse>('delete', `${environment.baseUrl}/admin/main_drink`, { body: data })
      .pipe(catchError(this.handleError));
  }
}
