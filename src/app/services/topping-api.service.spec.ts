import { TestBed } from '@angular/core/testing';

import { ToppingApiService } from './topping-api.service';

describe('ToppingApiService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ToppingApiService = TestBed.get(ToppingApiService);
    expect(service).toBeTruthy();
  });
});
