import { TestBed } from '@angular/core/testing';

import { MainDrinkApiService } from './main-drink-api.service';

describe('MainDrinkApiService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MainDrinkApiService = TestBed.get(MainDrinkApiService);
    expect(service).toBeTruthy();
  });
});
