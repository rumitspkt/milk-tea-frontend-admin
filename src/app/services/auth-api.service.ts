import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError, of } from 'rxjs';
import { environment } from 'src/environments/environment';
import { DataResponse } from '../classes/response';
import { catchError } from 'rxjs/operators';
import {  BaseService } from '../shared/services';

@Injectable({
  providedIn: 'root'
})
export class AuthApiService extends BaseService {

  constructor(private httpClient: HttpClient) {
    super();
  }

  login(data: any): Observable<DataResponse> {
    return this.httpClient.post<DataResponse>(`${environment.baseUrl}/auth/login`, data, {})
      .pipe(catchError(this.handleError));
  }

}
