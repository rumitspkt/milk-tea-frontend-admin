import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { BaseService } from '../shared/services';
import { catchError } from 'rxjs/operators';
import { DataResponse } from '../classes/response';

@Injectable({
  providedIn: 'root'
})
export class AccountApiService extends BaseService {

  constructor(private httpClient: HttpClient) {
    super();
  }

  getAccount(page: number, pageSize: number): Observable<DataResponse> {
    const url = `${environment.baseUrl}/admin/account?page=${page}&pageSize=${pageSize}`;
    return this.httpClient.get<DataResponse>(url).pipe(catchError(this.handleError));
  }

  createAccount(data: any): Observable<DataResponse> {
    return this.httpClient.post<DataResponse>(`${environment.baseUrl}/admin/account`, data, {})
      .pipe(catchError(this.handleError));
  }

  editAccount(data: any): Observable<DataResponse> {
    return this.httpClient.put<DataResponse>(`${environment.baseUrl}/admin/account`, data, {})
      .pipe(catchError(this.handleError));
  }

  deleteAccount(data: any): Observable<DataResponse> {
    return this.httpClient.request<DataResponse>('delete', `${environment.baseUrl}/admin/account`, { body: data })
      .pipe(catchError(this.handleError));
  }
}
