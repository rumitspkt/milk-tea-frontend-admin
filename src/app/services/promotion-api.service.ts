import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { BaseService } from '../shared/services';
import { DataResponse } from '../classes/response';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PromotionApiService extends BaseService {

  constructor(private httpClient: HttpClient) {
    super();
  }

  getPromotion(page: number, pageSize: number): Observable<DataResponse> {
    const url = `${environment.baseUrl}/admin/promotion?page=${page}&pageSize=${pageSize}`;
    return this.httpClient.get<DataResponse>(url).pipe(catchError(this.handleError));
  }

  createPromotion(data: any): Observable<DataResponse> {
    return this.httpClient.post<DataResponse>(`${environment.baseUrl}/admin/promotion`, data, {})
      .pipe(catchError(this.handleError));
  }

  editPromotion(data: any): Observable<DataResponse> {
    return this.httpClient.put<DataResponse>(`${environment.baseUrl}/admin/promotion`, data, {})
      .pipe(catchError(this.handleError));
  }

  deletePromotion(data: any): Observable<DataResponse> {
    return this.httpClient.request<DataResponse>('delete', `${environment.baseUrl}/admin/promotion`, { body: data })
      .pipe(catchError(this.handleError));
  }
}
