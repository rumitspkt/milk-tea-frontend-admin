import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { BaseService } from '../shared/services';
import { DataResponse } from '../classes/response';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class NotificationApiService extends BaseService {

  constructor(private httpClient: HttpClient) {
    super();
  }

  getNotification(page: number, pageSize: number): Observable<DataResponse> {
    const url = `${environment.baseUrl}/admin/notification?page=${page}&pageSize=${pageSize}`;
    return this.httpClient.get<DataResponse>(url).pipe(catchError(this.handleError));
  }

  createNotification(data: any): Observable<DataResponse> {
    return this.httpClient.post<DataResponse>(`${environment.baseUrl}/admin/notification`, data, {})
      .pipe(catchError(this.handleError));
  }

  editNotification(data: any): Observable<DataResponse> {
    return this.httpClient.put<DataResponse>(`${environment.baseUrl}/admin/notification`, data, {})
      .pipe(catchError(this.handleError));
  }

  deleteNotification(data: any): Observable<DataResponse> {
    return this.httpClient.request<DataResponse>('delete', `${environment.baseUrl}/admin/notification`, { body: data })
      .pipe(catchError(this.handleError));
  }
}
