import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { BaseService } from '../shared/services';
import { DataResponse } from '../classes/response';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ToppingApiService extends BaseService {

  constructor(private httpClient: HttpClient) {
    super();
  }

  getTopping(page: number, pageSize: number): Observable<DataResponse> {
    const url = `${environment.baseUrl}/admin/topping?page=${page}&pageSize=${pageSize}`;
    return this.httpClient.get<DataResponse>(url).pipe(catchError(this.handleError));
  }

  createTopping(data: any): Observable<DataResponse> {
    return this.httpClient.post<DataResponse>(`${environment.baseUrl}/admin/topping`, data, {})
      .pipe(catchError(this.handleError));
  }

  editTopping(data: any): Observable<DataResponse> {
    return this.httpClient.put<DataResponse>(`${environment.baseUrl}/admin/topping`, data, {})
      .pipe(catchError(this.handleError));
  }

  deleteTopping(data: any): Observable<DataResponse> {
    return this.httpClient.request<DataResponse>('delete', `${environment.baseUrl}/admin/topping`, { body: data })
      .pipe(catchError(this.handleError));
  }
}
