import { TestBed } from '@angular/core/testing';

import { PromotionApiService } from './promotion-api.service';

describe('PromotionApiService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PromotionApiService = TestBed.get(PromotionApiService);
    expect(service).toBeTruthy();
  });
});
