import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { BaseService } from '../shared/services';
import { DataResponse } from '../classes/response';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class OrderApiService extends BaseService {

  constructor(private httpClient: HttpClient) {
    super();
  }

  getOrder(page: number, pageSize: number): Observable<DataResponse> {
    const url = `${environment.baseUrl}/admin/order?page=${page}&pageSize=${pageSize}`;
    return this.httpClient.get<DataResponse>(url).pipe(catchError(this.handleError));
  }

  editOrder(data: any): Observable<DataResponse> {
    return this.httpClient.put<DataResponse>(`${environment.baseUrl}/admin/order`, data, {})
      .pipe(catchError(this.handleError));
  }

  deleteOrder(data: any): Observable<DataResponse> {
    return this.httpClient.request<DataResponse>('delete', `${environment.baseUrl}/admin/order`, { body: data })
      .pipe(catchError(this.handleError));
  }
}
