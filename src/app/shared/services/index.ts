import { HttpErrorResponse } from '@angular/common/http';
import { of } from 'rxjs';
import { DataResponse } from 'src/app/classes/response';

export class BaseService {
    protected handleError(error: HttpErrorResponse) {
        if (error.error instanceof ErrorEvent) {
            // A client-side or network error occurred. Handle it accordingly.
            return of({
                success: false,
                message: `client-side or network error: ${error.error.message}`
            } as DataResponse);
        } else {
            // The backend returned an unsuccessful response code.
            // The response body may contain clues as to what went wrong,
            return of({
                success: false,
                message: error.error ? error.error.message : null
            } as DataResponse);
        }
    }
}
