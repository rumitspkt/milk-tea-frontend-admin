import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MatSnackBar, MatTableDataSource, MatDialog, MatPaginator, MatSort, PageEvent } from '@angular/material';
import { Category } from 'src/app/classes/category';
import { MainDrinkApiService } from 'src/app/services/main-drink-api.service';
import { MainDrinkDialogComponent } from './main-drink-dialog/main-drink-dialog.component';
import { MainDrink } from 'src/app/classes/maindrink';
import { CategoryApiService } from 'src/app/services/category-api.service';

@Component({
  selector: 'app-main-drink',
  templateUrl: './main-drink.component.html',
  styleUrls: ['./main-drink.component.scss']
})
export class MainDrinkComponent implements OnInit {

  displayedColumns = ['_id', 'name', 'price', 'image_url'];
  dataSource = new MatTableDataSource();
  selectedCategoryId: string;
  categories: Category[];

  constructor(private mainDrinkApi: MainDrinkApiService, private dialog: MatDialog, private categoryApi: CategoryApiService) { }

  @ViewChild(MatPaginator) paginator: MatPaginator;

  @ViewChild(MatSort) sort: MatSort;

  ngOnInit() {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    this.categoryApi.getCategory(1, 20).subscribe(result => {
      if (result.success) {
        this.categories = result.result.docs;
      }
    });
  }

  onSelect() {
    this.loadData(this.selectedCategoryId);
  }

  loadData(categoryId: string) {
    this.mainDrinkApi.getMainDrink(categoryId).subscribe(data => {
      this.dataSource.paginator.length = data.result.length;
      this.dataSource.data = data.result;
    });
  }

  getData(event: PageEvent) {
    return event;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  addDialog() {
    const dialogRef = this.dialog.open(MainDrinkDialogComponent, {
      data: { action: 'add', mainDrink: new MainDrink(), categoryId: this.selectedCategoryId }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
      if (result) {
        this.loadData(this.selectedCategoryId);
      }
    });
  }

  detailDialog(mainDrink: any) {
    const dialogRef = this.dialog.open(MainDrinkDialogComponent, {
      data: { action: 'detail', mainDrink, categoryId: this.selectedCategoryId }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
      if (result) {
        this.loadData(this.selectedCategoryId);
      }
    });
  }

}
