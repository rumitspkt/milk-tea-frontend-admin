import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MainDrinkComponent } from './main-drink.component';

describe('MainDrinkComponent', () => {
  let component: MainDrinkComponent;
  let fixture: ComponentFixture<MainDrinkComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MainDrinkComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainDrinkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
