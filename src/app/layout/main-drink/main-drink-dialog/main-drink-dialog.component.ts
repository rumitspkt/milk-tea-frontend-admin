import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { Category } from 'src/app/classes/category';
import { MainDrinkApiService } from 'src/app/services/main-drink-api.service';
import { MainDrink } from 'src/app/classes/maindrink';

@Component({
  selector: 'app-main-drink-dialog',
  templateUrl: './main-drink-dialog.component.html',
  styleUrls: ['./main-drink-dialog.component.scss']
})
export class MainDrinkDialogComponent implements OnInit {

  isDetail: boolean;
  mainDrink: MainDrink;
  isEdit = false;
  categoryId: string;


  constructor(public dialogRef: MatDialogRef<MainDrinkDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, private maindrinkApi: MainDrinkApiService, private snackBar: MatSnackBar) {
    this.isDetail = 'detail' === data.action;
    this.mainDrink = data.mainDrink;
    this.categoryId = data.categoryId;
  }

  ngOnInit() {
  }

  onAdd() {
    this.maindrinkApi.createMainDrink({...this.mainDrink, categoryId: this.categoryId}).subscribe(result => {
      if (result.success) {
        this.snackBar.open(result.result);
        this.dialogRef.close('ok');
      } else {
        this.snackBar.open(result.message);
      }
    });
  }

  onDelete() {
    this.maindrinkApi.deleteMainDrink({ mainDrinkId: this.mainDrink._id, categoryId: this.categoryId }).subscribe(result => {
      if (result.success) {
        this.snackBar.open(result.result);
        this.dialogRef.close('ok');
      } else {
        this.snackBar.open(result.message);
      }
    });
  }

  onEdit() {
    if (!this.isEdit) {
      this.isEdit = true;
      this.isDetail = false;
      return;
    }
    this.maindrinkApi.editMainDrink({
      ...this.mainDrink,
      mainDrinkId: this.mainDrink._id,
      categoryId: this.categoryId
    }).subscribe(result => {
      if (result.success) {
        this.snackBar.open(result.result);
        this.dialogRef.close('ok');
      } else {
        this.snackBar.open(result.message);
      }
    });
  }

}
