import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MainDrinkDialogComponent } from './main-drink-dialog.component';

describe('MainDrinkDialogComponent', () => {
  let component: MainDrinkDialogComponent;
  let fixture: ComponentFixture<MainDrinkDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MainDrinkDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainDrinkDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
