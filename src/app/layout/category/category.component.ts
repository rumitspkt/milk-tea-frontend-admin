import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MatSnackBar, MatTableDataSource, MatDialog, MatPaginator, MatSort, PageEvent } from '@angular/material';
import { CategoryApiService } from 'src/app/services/category-api.service';
import { CategoryDialogComponent } from './category-dialog/category-dialog.component';
import { Category } from 'src/app/classes/category';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss']
})
export class CategoryComponent implements OnInit {
  displayedColumns = ['_id', 'name', 'code', 'image_url'];
  dataSource = new MatTableDataSource();

  constructor(private categoryApi: CategoryApiService, private dialog: MatDialog) { }

  @ViewChild(MatPaginator) paginator: MatPaginator;

  @ViewChild(MatSort) sort: MatSort;

  ngOnInit() {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    this.loadData();
  }

  loadData() {
    this.categoryApi.getCategory(1, 20).subscribe(data => {
      this.dataSource.paginator.length = data.result.total;
      this.dataSource.data = data.result.docs;
    });
  }

  getData(event: PageEvent) {
    return event;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  addDialog() {
    const dialogRef = this.dialog.open(CategoryDialogComponent, {
      data: { action: 'add', category: new Category() }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
      if (result) {
        this.loadData();
      }
    });
  }

  detailDialog(category: any) {
    const dialogRef = this.dialog.open(CategoryDialogComponent, {
      data: { action: 'detail', category }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
      if (result) {
        this.loadData();
      }
    });
  }

}
