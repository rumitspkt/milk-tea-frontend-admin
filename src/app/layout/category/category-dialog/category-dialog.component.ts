import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { CategoryApiService } from 'src/app/services/category-api.service';
import { Category } from 'src/app/classes/category';

@Component({
  selector: 'app-category-dialog',
  templateUrl: './category-dialog.component.html',
  styleUrls: ['./category-dialog.component.scss']
})
export class CategoryDialogComponent implements OnInit {

  isDetail: boolean;
  category: Category;
  isEdit = false;


  constructor(public dialogRef: MatDialogRef<CategoryDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, private categoryApi: CategoryApiService, private snackBar: MatSnackBar) {
    this.isDetail = 'detail' === data.action;
    this.category = data.category;
  }

  ngOnInit() {
  }

  onAdd() {
    this.categoryApi.createCategory(this.category).subscribe(result => {
      if (result.success) {
        this.snackBar.open(result.result);
        this.dialogRef.close('ok');
      } else {
        this.snackBar.open(result.message);
      }
    });
  }

  onDelete() {
    this.categoryApi.deleteCategory({ categoryId: this.category._id }).subscribe(result => {
      if (result.success) {
        this.snackBar.open(result.result);
        this.dialogRef.close('ok');
      } else {
        this.snackBar.open(result.message);
      }
    });
  }

  onEdit() {
    if (!this.isEdit) {
      this.isEdit = true;
      this.isDetail = false;
      return;
    }
    this.categoryApi.editCategory({ ...this.category, categoryId: this.category._id }).subscribe(result => {
      if (result.success) {
        this.snackBar.open(result.result);
        this.dialogRef.close('ok');
      } else {
        this.snackBar.open(result.message);
      }
    });
  }

}
