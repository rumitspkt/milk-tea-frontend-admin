import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MatSnackBar, MatTableDataSource, MatDialog, MatPaginator, MatSort, PageEvent } from '@angular/material';
import { NotificationApiService } from 'src/app/services/notification-api.service';
import { NotificationDialogComponent } from './notification-dialog/notification-dialog.component';
import { Notification } from 'src/app/classes/notification';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.scss']
})
export class NotificationComponent implements OnInit {

  displayedColumns = ['_id', 'title', 'content', 'image_url'];
  dataSource = new MatTableDataSource();

  constructor(private notificationApi: NotificationApiService, private dialog: MatDialog) { }

  @ViewChild(MatPaginator) paginator: MatPaginator;

  @ViewChild(MatSort) sort: MatSort;

  ngOnInit() {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    this.loadData();
  }

  loadData() {
    this.notificationApi.getNotification(1, 20).subscribe(data => {
      this.dataSource.paginator.length = data.result.total;
      this.dataSource.data = data.result.docs;
    });
  }

  getData(event: PageEvent) {
    return event;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  addDialog() {
    const dialogRef = this.dialog.open(NotificationDialogComponent, {
      data: { action: 'add', notification: new Notification() }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
      if (result) {
        this.loadData();
      }
    });
  }

  detailDialog(notification: any) {
    const dialogRef = this.dialog.open(NotificationDialogComponent, {
      data: { action: 'detail', notification }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
      if (result) {
        this.loadData();
      }
    });
  }

}
