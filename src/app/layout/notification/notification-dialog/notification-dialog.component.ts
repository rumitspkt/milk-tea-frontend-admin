import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { NotificationApiService } from 'src/app/services/notification-api.service';
import { Notification } from 'src/app/classes/notification';

@Component({
  selector: 'app-notification-dialog',
  templateUrl: './notification-dialog.component.html',
  styleUrls: ['./notification-dialog.component.scss']
})
export class NotificationDialogComponent implements OnInit {

  isDetail: boolean;
  notification: Notification;
  isEdit = false;


  constructor(public dialogRef: MatDialogRef<NotificationDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, private notificationApi: NotificationApiService, private snackBar: MatSnackBar) {
    this.isDetail = 'detail' === data.action;
    this.notification = data.notification;
  }

  ngOnInit() {
  }

  onAdd() {
    this.notificationApi.createNotification(this.notification).subscribe(result => {
      if (result.success) {
        this.snackBar.open(result.result);
        this.dialogRef.close('ok');
      } else {
        this.snackBar.open(result.message);
      }
    });
  }

  onDelete() {
    this.notificationApi.deleteNotification({ notificationId: this.notification._id }).subscribe(result => {
      if (result.success) {
        this.snackBar.open(result.result);
        this.dialogRef.close('ok');
      } else {
        this.snackBar.open(result.message);
      }
    });
  }

  onEdit() {
    if (!this.isEdit) {
      this.isEdit = true;
      this.isDetail = false;
      return;
    }
    this.notificationApi.editNotification({ ...this.notification, notificationId: this.notification._id }).subscribe(result => {
      if (result.success) {
        this.snackBar.open(result.result);
        this.dialogRef.close('ok');
      } else {
        this.snackBar.open(result.message);
      }
    });
  }

}
