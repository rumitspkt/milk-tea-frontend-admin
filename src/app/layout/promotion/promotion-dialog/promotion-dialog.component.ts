import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar, MatDatepickerInputEvent } from '@angular/material';
import { Promotion } from 'src/app/classes/promotion';
import { PromotionApiService } from 'src/app/services/promotion-api.service';

@Component({
  selector: 'app-promotion-dialog',
  templateUrl: './promotion-dialog.component.html',
  styleUrls: ['./promotion-dialog.component.scss']
})
export class PromotionDialogComponent implements OnInit {

  isDetail: boolean;
  promotion: Promotion;
  isEdit = false;


  constructor(public dialogRef: MatDialogRef<PromotionDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, private promotionApi: PromotionApiService, private snackBar: MatSnackBar) {
    this.isDetail = 'detail' === data.action;
    this.promotion = data.promotion;
  }

  ngOnInit() {
  }

  onAdd() {
    this.promotionApi.createPromotion(this.promotion).subscribe(result => {
      if (result.success) {
        this.snackBar.open(result.result);
        this.dialogRef.close('ok');
      } else {
        this.snackBar.open(result.message);
      }
    });
  }

  onDelete() {
    this.promotionApi.deletePromotion({ promotionId: this.promotion._id }).subscribe(result => {
      if (result.success) {
        this.snackBar.open(result.result);
        this.dialogRef.close('ok');
      } else {
        this.snackBar.open(result.message);
      }
    });
  }

  onEdit() {
    if (!this.isEdit) {
      this.isEdit = true;
      this.isDetail = false;
      return;
    }
    this.promotionApi.editPromotion({ ...this.promotion, promotionId: this.promotion._id }).subscribe(result => {
      if (result.success) {
        this.snackBar.open(result.result);
        this.dialogRef.close('ok');
      } else {
        this.snackBar.open(result.message);
      }
    });
  }

}
