import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MatSnackBar, MatTableDataSource, MatDialog, MatPaginator, MatSort, PageEvent } from '@angular/material';
import { PromotionApiService } from 'src/app/services/promotion-api.service';
import { Promotion } from 'src/app/classes/promotion';
import { PromotionDialogComponent } from './promotion-dialog/promotion-dialog.component';

@Component({
  selector: 'app-promotion',
  templateUrl: './promotion.component.html',
  styleUrls: ['./promotion.component.scss']
})
export class PromotionComponent implements OnInit {

  displayedColumns = ['_id', 'code', 'title', 'value', 'image_url'];
  dataSource = new MatTableDataSource();

  constructor(private promotionApi: PromotionApiService, private dialog: MatDialog) { }

  @ViewChild(MatPaginator) paginator: MatPaginator;

  @ViewChild(MatSort) sort: MatSort;

  ngOnInit() {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    this.loadData();
  }

  loadData() {
    this.promotionApi.getPromotion(1, 20).subscribe(data => {
      this.dataSource.paginator.length = data.result.total;
      this.dataSource.data = data.result.docs;
    });
  }

  getData(event: PageEvent) {
    return event;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  addDialog() {
    const dialogRef = this.dialog.open(PromotionDialogComponent, {
      data: { action: 'add', promotion: new Promotion() }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
      if (result) {
        this.loadData();
      }
    });
  }

  detailDialog(promotion: any) {
    const dialogRef = this.dialog.open(PromotionDialogComponent, {
      data: { action: 'detail', promotion }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
      if (result) {
        this.loadData();
      }
    });
  }

}
