import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import {
    MatButtonModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatSidenavModule,
    MatToolbarModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatDialogModule,
    MatSelectModule,
    MatDatepickerModule
} from '@angular/material';
import { TranslateModule } from '@ngx-translate/core';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { TopnavComponent } from './components/topnav/topnav.component';
import { LayoutRoutingModule } from './layout-routing.module';
import { LayoutComponent } from './layout.component';
import { NavComponent } from './nav/nav.component';
import { Screen2Component } from './screen2/screen2.component';
import { StaffComponent } from './staff/staff.component';
import { CustomerComponent } from './customer/customer.component';
import { MainDrinkComponent } from './main-drink/main-drink.component';
import { ToppingComponent } from './topping/topping.component';
import { OrderComponent } from './order/order.component';
import { PromotionComponent } from './promotion/promotion.component';
import { NotificationComponent } from './notification/notification.component';
import { CategoryComponent } from './category/category.component';
import { SnackSuccessComponent } from './components/snack-success/snack-success.component';
import { SnackErrorComponent } from './components/snack-error/snack-error.component';
import { SnackLoadingComponent } from './components/snack-loading/snack-loading.component';
import { StaffDialogComponent } from './staff/staff-dialog/staff-dialog.component';
import { FormsModule } from '@angular/forms';
import { CustomerDialogComponent } from './customer/customer-dialog/customer-dialog.component';
import { CategoryDialogComponent } from './category/category-dialog/category-dialog.component';
import { MainDrinkDialogComponent } from './main-drink/main-drink-dialog/main-drink-dialog.component';
import { ToppingDialogComponent } from './topping/topping-dialog/topping-dialog.component';
import { PromotionDialogComponent } from './promotion/promotion-dialog/promotion-dialog.component';
import { NotificationDialogComponent } from './notification/notification-dialog/notification-dialog.component';
import { OrderDialogComponent } from './order/order-dialog/order-dialog.component';

@NgModule({
    imports: [
        CommonModule,
        LayoutRoutingModule,
        MatToolbarModule,
        MatButtonModule,
        MatSidenavModule,
        MatIconModule,
        MatInputModule,
        MatMenuModule,
        MatListModule,
        MatTableModule,
        MatPaginatorModule,
        MatSortModule,
        MatDialogModule,
        MatSelectModule,
        MatDatepickerModule,
        FormsModule,
        TranslateModule
    ],
    declarations: [Screen2Component, LayoutComponent, NavComponent, TopnavComponent, SidebarComponent, StaffComponent, CustomerComponent, MainDrinkComponent, ToppingComponent, OrderComponent, PromotionComponent, NotificationComponent, CategoryComponent, StaffDialogComponent, CustomerDialogComponent, CategoryDialogComponent, MainDrinkDialogComponent, ToppingDialogComponent, PromotionDialogComponent, NotificationDialogComponent, OrderDialogComponent, ],
    entryComponents: [StaffDialogComponent, CustomerDialogComponent, CategoryDialogComponent, MainDrinkDialogComponent, ToppingDialogComponent, PromotionDialogComponent, NotificationDialogComponent]
})
export class LayoutModule { }
