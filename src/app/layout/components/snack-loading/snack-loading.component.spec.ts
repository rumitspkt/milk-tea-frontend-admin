import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SnackLoadingComponent } from './snack-loading.component';

describe('SnackLoadingComponent', () => {
  let component: SnackLoadingComponent;
  let fixture: ComponentFixture<SnackLoadingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SnackLoadingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SnackLoadingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
