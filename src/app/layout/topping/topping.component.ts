import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MatSnackBar, MatTableDataSource, MatDialog, MatPaginator, MatSort, PageEvent } from '@angular/material';
import { ToppingApiService } from 'src/app/services/topping-api.service';
import { Topping } from 'src/app/classes/topping';
import { ToppingDialogComponent } from './topping-dialog/topping-dialog.component';

@Component({
  selector: 'app-topping',
  templateUrl: './topping.component.html',
  styleUrls: ['./topping.component.scss']
})
export class ToppingComponent implements OnInit {
  displayedColumns = ['_id', 'name', 'code', 'price'];
  dataSource = new MatTableDataSource();

  constructor(private toppingApi: ToppingApiService, private dialog: MatDialog) { }

  @ViewChild(MatPaginator) paginator: MatPaginator;

  @ViewChild(MatSort) sort: MatSort;

  ngOnInit() {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    this.loadData();
  }

  loadData() {
    this.toppingApi.getTopping(1, 20).subscribe(data => {
      this.dataSource.paginator.length = data.result.total;
      this.dataSource.data = data.result.docs;
    });
  }

  getData(event: PageEvent) {
    return event;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  addDialog() {
    const dialogRef = this.dialog.open(ToppingDialogComponent, {
      data: { action: 'add', topping: new Topping() }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
      if (result) {
        this.loadData();
      }
    });
  }

  detailDialog(topping: any) {
    const dialogRef = this.dialog.open(ToppingDialogComponent, {
      data: { action: 'detail', topping }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
      if (result) {
        this.loadData();
      }
    });
  }

}
