import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ToppingDialogComponent } from './topping-dialog.component';

describe('ToppingDialogComponent', () => {
  let component: ToppingDialogComponent;
  let fixture: ComponentFixture<ToppingDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ToppingDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ToppingDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
