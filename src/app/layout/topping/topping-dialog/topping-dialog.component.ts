import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { Topping } from 'src/app/classes/topping';
import { ToppingApiService } from 'src/app/services/topping-api.service';

@Component({
  selector: 'app-topping-dialog',
  templateUrl: './topping-dialog.component.html',
  styleUrls: ['./topping-dialog.component.scss']
})
export class ToppingDialogComponent implements OnInit {

  isDetail: boolean;
  topping: Topping;
  isEdit = false;


  constructor(public dialogRef: MatDialogRef<ToppingDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, private toppingApi: ToppingApiService, private snackBar: MatSnackBar) {
    this.isDetail = 'detail' === data.action;
    this.topping = data.topping;
  }

  ngOnInit() {
  }

  onAdd() {
    this.toppingApi.createTopping(this.topping).subscribe(result => {
      if (result.success) {
        this.snackBar.open(result.result);
        this.dialogRef.close('ok');
      } else {
        this.snackBar.open(result.message);
      }
    });
  }

  onDelete() {
    this.toppingApi.deleteTopping({ toppingId: this.topping._id }).subscribe(result => {
      if (result.success) {
        this.snackBar.open(result.result);
        this.dialogRef.close('ok');
      } else {
        this.snackBar.open(result.message);
      }
    });
  }

  onEdit() {
    if (!this.isEdit) {
      this.isEdit = true;
      this.isDetail = false;
      return;
    }
    this.toppingApi.editTopping({ ...this.topping, toppingId: this.topping._id }).subscribe(result => {
      if (result.success) {
        this.snackBar.open(result.result);
        this.dialogRef.close('ok');
      } else {
        this.snackBar.open(result.message);
      }
    });
  }

}
