import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator, PageEvent, MatSort, MatDialog } from '@angular/material';
import { AccountApiService } from 'src/app/services/account-api.service';
import { Account } from 'src/app/classes/account';
import { CustomerDialogComponent } from './customer-dialog/customer-dialog.component';

export interface DataResponse {
  success: boolean;
  result: {
    docs: Account[];
  };
}

@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.scss']
})
export class CustomerComponent implements OnInit {
  displayedColumns = ['_id', 'username', 'role', 'phone', 'email', 'address'];
  dataSource = new MatTableDataSource();

  constructor(private accountApiService: AccountApiService, private dialog: MatDialog) { }

  @ViewChild(MatPaginator) paginator: MatPaginator;

  @ViewChild(MatSort) sort: MatSort;

  ngOnInit() {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    this.loadData();
  }

  loadData() {
    this.accountApiService.getAccount(1, 20).subscribe(data => {
      this.dataSource.paginator.length = data.result.total;
      this.dataSource.data = data.result.docs;
    });
  }

  getData(event: PageEvent) {
    return event;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  addDialog() {
    const dialogRef = this.dialog.open(CustomerDialogComponent, {
      data: { action: 'add', account: new Account() }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
      if (result) {
        this.loadData();
      }
    });
  }

  detailDialog(account: any) {
    const dialogRef = this.dialog.open(CustomerDialogComponent, {
      data: { action: 'detail', account }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
      if (result) {
        this.loadData();
      }
    });
  }

}
