import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { Account } from 'src/app/classes/account';
import { AccountApiService } from 'src/app/services/account-api.service';

@Component({
  selector: 'app-staff-dialog',
  templateUrl: './staff-dialog.component.html',
  styleUrls: ['./staff-dialog.component.scss']
})
export class StaffDialogComponent implements OnInit {

  isDetail: boolean;
  account: Account;
  isEdit = false;


  constructor(public dialogRef: MatDialogRef<StaffDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, private accountApi: AccountApiService, private snackBar: MatSnackBar) {
    this.isDetail = 'detail' === data.action;
    this.account = data.account;
  }

  ngOnInit() {
  }

  onAdd() {
    this.account.role = 'staff';
    this.accountApi.createAccount(this.account).subscribe(result => {
      if (result.success) {
        this.snackBar.open(result.result);
        this.dialogRef.close('ok');
      } else {
        this.snackBar.open(result.message);
      }
    });
  }

  onDelete() {
    this.accountApi.deleteAccount({ userId: this.account._id }).subscribe(result => {
      if (result.success) {
        this.snackBar.open(result.result);
        this.dialogRef.close('ok');
      } else {
        this.snackBar.open(result.message);
      }
    });
  }

  onEdit() {
    if (!this.isEdit) {
      this.isEdit = true;
      this.isDetail = false;
      return;
    }
    this.accountApi.editAccount({ ...this.account, userId: this.account._id }).subscribe(result => {
      if (result.success) {
        this.snackBar.open(result.result);
        this.dialogRef.close('ok');
      } else {
        this.snackBar.open(result.message);
      }
    });
  }

}
