import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LayoutComponent } from './layout.component';
import { Screen1Component } from './screen1/screen1.component';
import { Screen2Component } from './screen2/screen2.component';
import { StaffComponent } from './staff/staff.component';
import { CategoryComponent } from './category/category.component';
import { CustomerComponent } from './customer/customer.component';
import { MainDrinkComponent } from './main-drink/main-drink.component';
import { NotificationComponent } from './notification/notification.component';
import { OrderComponent } from './order/order.component';
import { PromotionComponent } from './promotion/promotion.component';
import { ToppingComponent } from './topping/topping.component';

const routes: Routes = [
    {
        path: '',
        component: LayoutComponent,
        children: [
            {
                path: '',
                redirectTo: 'dashboard'
            },
            {
                path: 'dashboard',
                loadChildren: './dashboard/dashboard.module#DashboardModule'
            },
            {
                path: 'screen1',
                loadChildren: './screen1/screen1.module#Screen1Module'
            },
            {
                path: 'screen2',
                component: Screen2Component
            },
            {
                path: 'staff',
                component: StaffComponent
            },
            {
                path: 'category',
                component: CategoryComponent
            },
            {
                path: 'customer',
                component: CustomerComponent
            },
            {
                path: 'main-drink',
                component: MainDrinkComponent
            },
            {
                path: 'notification',
                component: NotificationComponent
            },
            {
                path: 'order',
                component: OrderComponent
            },
            {
                path: 'promotion',
                component: PromotionComponent
            },
            {
                path: 'topping',
                component: ToppingComponent
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class LayoutRoutingModule {}
